#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Install expect interpreter if not already present.
if [ -z $(command -v expect) ]; then
	sudo apt update && sudo apt -y install expect
fi

sudo ln -sf ${DIR}/logmein.sh /usr/local/bin/logmein

echo -e '\nAll done, you can type "logmein <pw-file-path> <cmd>".\n'